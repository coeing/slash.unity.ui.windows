﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InventoryView.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory
{
    using System.Linq;

    using Slash.Unity.UI.Windows.Example.Game;
    using Slash.Unity.UI.Windows.Example.Inventory.DragDrop;

    using UnityEngine;

    public class InventoryView : MonoBehaviour
    {
        #region Fields

        public GameBehaviour Game;

        public string InventoryWindowId;

        private EquipmentContext equipmentContext;

        private InventoryContext inventoryContext;

        private Window window;

        #endregion

        #region Public Methods and Operators

        public void OpenInventoryWindow()
        {
            // Check if already open.
            if (this.window != null)
            {
                Debug.LogWarning("Inventory already open.");
                return;
            }

            // Open window.
            if (WindowManager.Instance != null)
            {
                if (!string.IsNullOrEmpty(this.InventoryWindowId))
                {
                    // Create context for window.
                    var windowContext = new InventoryWindowContext
                    {
                        Inventory = this.inventoryContext,
                        Equipment = this.equipmentContext
                    };
                    windowContext.Drop += this.OnEquip;
                    windowContext.Close += this.OnClose;
                    this.window = WindowManager.Instance.OpenWindow(this.InventoryWindowId, windowContext);
                }
                else
                {
                    Debug.LogWarning("No window id set.", this);
                }
            }
            else
            {
                Debug.LogWarning("No window manager found.", this);
            }
        }

        #endregion

        #region Methods

        protected void Start()
        {
            if (this.Game == null)
            {
                Debug.LogWarning("No game set");
                return;
            }

            var gameLogic = this.Game.GameLogic;

            // Register for events.
            gameLogic.InventoryItemAdded += this.OnInventoryItemAdded;
            gameLogic.InventoryItemRemoved += this.OnInventoryItemRemoved;
            gameLogic.InventoryItemAmountIncreased += this.OnInventoryItemAmountIncreased;
            gameLogic.InventoryItemAmountDecreased += this.OnInventoryItemAmountDecreased;

            gameLogic.EquipmentSlotChanged += this.OnEquipmentSlotChanged;

            // Initialize inventory context.
            var inventory = gameLogic.Inventory;
            this.inventoryContext = new InventoryContext();
            foreach (var inventoryItem in inventory.Items)
            {
                this.inventoryContext.Slots.Add(
                    new InventoryItemContext() { TypeId = inventoryItem.TypeId, Amount = inventoryItem.Amount });
            }

            this.equipmentContext = new EquipmentContext();
            var equipment = gameLogic.Equipment;

            string slotItemTypeId;
            equipment.Slots.TryGetValue(EquipmentSlot.LeftHand, out slotItemTypeId);
            this.equipmentContext.LeftHandSlot = new EquipmentSlotContext()
            {
                Slot = EquipmentSlot.LeftHand,
                TypeId = slotItemTypeId
            };

            equipment.Slots.TryGetValue(EquipmentSlot.RightHand, out slotItemTypeId);
            this.equipmentContext.RightHandSlot = new EquipmentSlotContext()
            {
                Slot = EquipmentSlot.RightHand,
                TypeId = slotItemTypeId
            };
        }

        private EquipmentSlotContext GetEquipmentSlotContext(EquipmentSlot slot)
        {
            switch (slot)
            {
                case EquipmentSlot.LeftHand:
                    return this.equipmentContext.LeftHandSlot;
                case EquipmentSlot.RightHand:
                    return this.equipmentContext.RightHandSlot;
            }
            return null;
        }

        private InventoryItemContext GetInventoryItemContext(string itemTypeId)
        {
            return
                this.inventoryContext.Slots.FirstOrDefault(
                    existingInventoryItemContext => existingInventoryItemContext.TypeId == itemTypeId);
        }

        private void OnClose()
        {
            if (this.window == null)
            {
                return;
            }

            // Destroy window context.
            var windowContext = (InventoryWindowContext)this.window.Context;
            windowContext.Drop -= this.OnEquip;
            windowContext.Close -= this.OnClose;

            // Close window.
            WindowManager.Instance.CloseWindow(this.window);
            this.window = null;
        }

        private void OnEquip(EquipmentSlotContext equipmentSlot, ItemDragData itemDragData)
        {
            if (itemDragData == null)
            {
                return;
            }

            this.Game.GameLogic.EquipItem(equipmentSlot.Slot, itemDragData.Item.TypeId);
        }

        private void OnEquipmentSlotChanged(EquipmentSlot slot, string itemTypeId)
        {
            this.UpdateEquipementSlot(slot, itemTypeId);
        }

        private void OnInventoryItemAdded(string itemTypeId)
        {
            var inventoryItemContext = new InventoryItemContext { TypeId = itemTypeId };
            this.inventoryContext.Slots.Add(inventoryItemContext);
        }

        private void OnInventoryItemAmountDecreased(string itemTypeId)
        {
            var inventoryItemContext = this.GetInventoryItemContext(itemTypeId);
            if (inventoryItemContext != null)
            {
                inventoryItemContext.Amount -= 1;
            }
        }

        private void OnInventoryItemAmountIncreased(string itemTypeId)
        {
            var inventoryItemContext = this.GetInventoryItemContext(itemTypeId);
            if (inventoryItemContext != null)
            {
                inventoryItemContext.Amount += 1;
            }
        }

        private void OnInventoryItemRemoved(string itemTypeId)
        {
            var inventoryItemContext = this.GetInventoryItemContext(itemTypeId);
            if (inventoryItemContext != null)
            {
                this.inventoryContext.Slots.Remove(inventoryItemContext);
            }
        }

        private void UpdateEquipementSlot(EquipmentSlot slot, string itemTypeId)
        {
            var equipmentSlotContext = this.GetEquipmentSlotContext(slot);
            if (equipmentSlotContext != null)
            {
                equipmentSlotContext.TypeId = itemTypeId;
            }
        }

        #endregion
    }
}