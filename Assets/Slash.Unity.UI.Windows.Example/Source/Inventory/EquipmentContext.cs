﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EquipmentContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory
{
    using Slash.Unity.DataBind.Core.Data;

    public class EquipmentContext : Context
    {
        #region Fields

        private readonly Property<EquipmentSlotContext> leftHandSlotProperty = new Property<EquipmentSlotContext>();

        private readonly Property<EquipmentSlotContext> rightHandSlotProperty = new Property<EquipmentSlotContext>();

        #endregion

        #region Properties

        public EquipmentSlotContext LeftHandSlot
        {
            get
            {
                return this.leftHandSlotProperty.Value;
            }
            set
            {
                this.leftHandSlotProperty.Value = value;
            }
        }

        public EquipmentSlotContext RightHandSlot
        {
            get
            {
                return this.rightHandSlotProperty.Value;
            }
            set
            {
                this.rightHandSlotProperty.Value = value;
            }
        }

        #endregion
    }
}