﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemDragData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory.DragDrop
{
    public class ItemDragData
    {
        #region Properties

        public InventoryItemContext Item { get; set; }

        #endregion
    }
}