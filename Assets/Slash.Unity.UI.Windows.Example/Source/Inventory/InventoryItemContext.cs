﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InventoryItemContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory
{
    using Slash.Unity.DataBind.Core.Data;

    public class InventoryItemContext : Context
    {
        #region Fields

        private readonly Property<int> amountProperty = new Property<int>();

        private readonly Property<string> typeIdProperty = new Property<string>();

        #endregion

        #region Properties

        public int Amount
        {
            get
            {
                return this.amountProperty.Value;
            }
            set
            {
                this.amountProperty.Value = value;
            }
        }

        public string TypeId
        {
            get
            {
                return this.typeIdProperty.Value;
            }
            set
            {
                this.typeIdProperty.Value = value;
            }
        }

        #endregion
    }
}