﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EquipmentSlotContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory
{
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.UI.Windows.Example.Game;

    public class EquipmentSlotContext : Context
    {
        #region Fields

        private readonly Property<EquipmentSlot> slotProperty = new Property<EquipmentSlot>();

        private readonly Property<string> typeIdProperty = new Property<string>();

        #endregion

        #region Properties

        public EquipmentSlot Slot
        {
            get
            {
                return this.slotProperty.Value;
            }
            set
            {
                this.slotProperty.Value = value;
            }
        }

        public string TypeId
        {
            get
            {
                return this.typeIdProperty.Value;
            }
            set
            {
                this.typeIdProperty.Value = value;
            }
        }

        #endregion
    }
}