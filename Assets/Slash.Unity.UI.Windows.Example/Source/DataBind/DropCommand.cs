﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.DataBind
{
    using Slash.Unity.DataBind.UI.Unity.Commands;
    using Slash.Unity.Input.DragDrop;
    using Slash.Unity.Input.Handlers;

    using UnityEngine.Events;

    public class DropCommand : UnityEventCommand<DropOperationHandler, DragDropOperation>
    {
        #region Methods

        protected override UnityEvent<DragDropOperation> GetEvent(DropOperationHandler target)
        {
            return target.Drop;
        }

        protected override void OnEvent(DragDropOperation dragDropOperation)
        {
            this.InvokeCommand(dragDropOperation);
        }

        #endregion
    }
}