﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Game
{
    using UnityEngine;

    public class GameBehaviour : MonoBehaviour
    {
        #region Properties

        public GameLogic GameLogic { get; private set; }

        #endregion

        #region Methods

        protected void Awake()
        {
            this.GameLogic = new GameLogic();
        }

        protected void Update()
        {
            this.GameLogic.Update(Time.deltaTime);
        }

        #endregion
    }
}